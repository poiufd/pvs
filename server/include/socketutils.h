#ifndef SOCKETUTILS_H
#define SOCKETUTILS_H

#include <time.h>
#include <sys/queue.h>

#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <sys/sysinfo.h>
#include <mqueue.h>
#include <stdbool.h>

#define DATASIZE 1000
#define BUFSIZE 1000
#define TIMEOUT 120
          
struct socket_info
{
	LIST_ENTRY(socket_info) entries;
	int fd;
	time_t arrival_time;
	char* data;
	int data_length;
	char output[1024];
	char state[5];
	char *username[10];
	int n;
};

LIST_HEAD(listhead, socket_info);

void create_server_socket(int*, int);
void close_sockets(fd_set*,fd_set*, struct listhead *);
int read_socket(struct listhead *, int);
int write_socket(struct listhead *, int);
void check_socket_timeout(fd_set*, fd_set*, struct listhead *);
void fill_socket_context(struct listhead *, int);
void delete_from_list(struct listhead *, int);

#endif