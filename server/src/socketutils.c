#include "socketutils.h"
#include "smtp.h"

void close_sockets(fd_set* read_set, fd_set* write_set, struct listhead *h)
{
    struct socket_info *sk_p;

    while (!LIST_EMPTY(h)) 
    {
        sk_p = LIST_FIRST(h);

        close(sk_p->fd);
        FD_CLR(sk_p->fd, read_set);
        if (FD_ISSET(sk_p->fd, write_set))
            FD_CLR(sk_p->fd, write_set);
        printf("client socket closed\n"); 

        LIST_REMOVE(sk_p, entries);
        free(sk_p->data);
        for (int i = 0; i < 10;i++)
            free(sk_p->username[i]);
        free(sk_p);
    }
}

int write_socket(struct listhead *h, int sk)
{
    int retval;
    struct socket_info *sk_p;

    LIST_FOREACH(sk_p, h, entries)
        if (sk_p->fd == sk)
        {
            retval = send(sk, sk_p->output, sizeof(sk_p->output), 0);
            if (retval < 0)
            {
                if (errno == EWOULDBLOCK)
                    return -1;
                else
                {
                    perror("send");
                    return -1;
                }
            }
        }
    return 1;
}

int read_socket(struct listhead *h, int sk)
{
    char buf[BUFSIZE];
    char send_buf[1024];
    int len;
    int retval;
    struct socket_info *sk_p;

    LIST_FOREACH(sk_p, h, entries)
    {
        if (sk_p->fd == sk)
            break;
    }
    //receive message from socket 
    len = recv(sk, buf, BUFSIZE, 0);
    if (len < 0) 
    {
        perror("recv");
        return -1;
    }
    else if (len == 0) 
    {
        printf("Remote host has closed the connection\n");
        return -1;
    }

    buf[len] = '\0';
    printf("Received buffer = %s\n", buf); 
    if ((buf[len-2] == '\r') && (buf[len-1] == '\n'))
    {
        printf("\\r\\n found\n");
        //check if we have smth already stored
        int length = strlen(sk_p->data);
        if (length > 0)
        {
            if ((length+len) > sk_p->data_length) 
            {
                sk_p->data = realloc(sk_p->data,sk_p->data_length+2*DATASIZE);
                sk_p->data_length+=DATASIZE;
            }
            strcat(sk_p->data,buf);
            parce_command(sk_p->data,send_buf,sk_p);
        }
        else
        {
            parce_command(buf,send_buf,sk_p);
        }
        printf("sending answer %s\n",send_buf);


        retval = send(sk, send_buf, strlen(send_buf), 0);
        if (retval < 0)
        {
            if (errno == EWOULDBLOCK)
            {
                strcpy(sk_p->output,send_buf);
                return  0;
            }
            else
            {
                perror("send");
                return -1;
            }
        } 
    }
    //no command end
    else
    {
        int l = strlen(sk_p->data);
        if ((l+len) >= sk_p->data_length) 
        {
            printf("data length %d\n",sk_p->data_length);
            printf("new data length %d\n",sk_p->data_length+ 2*DATASIZE);
            sk_p->data = realloc(sk_p->data,sk_p->data_length+2*DATASIZE);
            sk_p->data_length+=DATASIZE;
            printf("memory realloced\n");
        }
        strcat(sk_p->data,buf);
                                 
    }      
    return 1; 
}

void check_socket_timeout(fd_set* read_set, fd_set* write_set, struct listhead *h)
{
    struct socket_info *n1,*n2;

    if LIST_EMPTY(h)
        return;
    n1 = LIST_FIRST(h);  
    while (n1 != NULL) 
    {
        n2 = LIST_NEXT(n1, entries);
        if (abs(n1->arrival_time - time(NULL)) >= TIMEOUT)
        {
            close(n1->fd);
            FD_CLR(n1->fd, read_set);
            if (FD_ISSET(n1->fd, write_set))
                FD_CLR(n1->fd, write_set);
            LIST_REMOVE(n1, entries);
            free(n1->data);
            for (int i = 0; i < 10;i++)
                free(n1->username[i]);
            free(n1);
            printf("client socket timeout exceeded\n"); 
        }
        n1 = n2;
    }
}

void fill_socket_context(struct listhead *h, int fd)
{
    struct socket_info *sk;
    sk = malloc(sizeof(struct socket_info));
    sk->fd = fd;
    sk->arrival_time = time(NULL);
    sk->data = malloc(sizeof(char)*DATASIZE);
    sk->data[0] = '\0';
    sk->data_length = DATASIZE;
    sk->n = 0;
    for(int i = 0; i < 10; i++)
        sk->username[i] = malloc(sizeof(char)*100);
    LIST_INSERT_HEAD(h, sk, entries);   
}

void delete_from_list(struct listhead *h, int fd)
{
    struct socket_info *sk_p;

    LIST_FOREACH(sk_p, h, entries)
    {
        if (sk_p->fd == fd)
        {
            LIST_REMOVE(sk_p, entries);
            printf("socket fd deleted from list\n");
            free(sk_p->data);
            for(int i=0; i<10;i++)
                free(sk_p->username[i]);
            free(sk_p);
            break;
        }
    }
}

void create_server_socket(int* server_sk, int port)
{
    //struct sockaddr_in server_addr;
    struct sockaddr_in6 server_addr;
    int on = 1;    
    //create server socket
    if ((*server_sk = socket(AF_INET6, SOCK_STREAM, 0)) < 0) {
        perror("socket");
        exit(1);
    }
    //address and port
    memset(&server_addr, 0, sizeof(server_addr));
    //server_addr.sin_family = AF_INET;
    //server_addr.sin_port = htons(port);
    //server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin6_family = AF_INET6;
    server_addr.sin6_port   = htons(port);   
    server_addr.sin6_addr   = in6addr_any;
    //allow reusing addresses
    if (setsockopt(*server_sk, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(int)) < 0)
    {
        perror("reuseaddr");
        exit(1);
    }
    //enable async mode
    if (ioctl(*server_sk, FIOASYNC, (char *)&on) < 0)
    {
        perror("async");
        exit(1);
    }
    //bind to address and port
    if (bind(*server_sk, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0) 
    {
        perror("bind");
        exit(1);
    }
    //wait for connections
    if (listen(*server_sk, SOMAXCONN) < 0)
    {
        perror("listen");
        exit(1);
    };
}