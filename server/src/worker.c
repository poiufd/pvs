#include "socketutils.h"

int sig_num;
static void handler(int sig) { sig_num = sig; }

void* thread_handler(void* fd_p)
{ 
    mqd_t fd = *(mqd_t*)fd_p;
    fd_set read_set, read_ready, write_set, write_ready;
    int sk;
    struct sigaction sa;
    sigset_t emptyset;
    struct timespec timeout;

    struct listhead head = LIST_HEAD_INITIALIZER(head);
    LIST_INIT(&head);

    printf("thread started\n");
    //establish signal handler
    sa.sa_handler = handler;        
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);
    sigaction(SIGUSR1, &sa, NULL);

    FD_ZERO(&read_set);
    FD_ZERO(&write_set);
    FD_SET(fd, &read_set);

    sigemptyset(&emptyset);
    timeout.tv_sec = TIMEOUT;
    timeout.tv_nsec = 0;

    int retval;
    while (1) 
    {
        read_ready = read_set; 
        write_ready = write_set;
        retval = pselect(FD_SETSIZE, &read_ready, &write_ready, NULL, &timeout, &emptyset);       
        //signal caught
        if (retval < 0) 
        {
            if (errno == EINTR && sig_num == SIGUSR1)
            {
                printf("SIGINT handling in thread...\n");
                FD_CLR(fd,&read_set);
                close_sockets(&read_set, &write_set, &head);
                break;
            } 
            else
                perror("pselect"); 
        }
        //timeout
        else if (retval == 0)
        {
            check_socket_timeout(&read_set, &write_set, &head);
            printf("timeout\n");
        }
        //ready descriptors
        else
        {
            for (sk = 0; sk < FD_SETSIZE; sk++) 
            {
                if (FD_ISSET(sk, &read_ready)) 
                {
                    if (sk == fd)
                    {
                        //queue ready for read
                        char buf[5];
                        if (mq_receive(fd, buf, sizeof(buf), 0) < 0)
                            perror("mq_receive");
                        else
                        {
                            FD_SET(atoi(buf),&read_set);
                            fill_socket_context(&head,atoi(buf));
                        }
                    }
                    else 
                    {
                        retval = read_socket(&head,sk);
                        if (retval < 0)
                        {
                            close(sk);
                            FD_CLR(sk, &read_set);
                            if FD_ISSET(sk,&write_set)
                                FD_CLR(sk,&write_set);
                            delete_from_list(&head,sk);
                        } 
                        else if (retval == 0)
                        {
                            FD_SET(sk,&write_set);
                        }                      
                    }
                }
                if (FD_ISSET(sk, &write_ready))
                {
                    if (write_socket(&head,sk))
                        FD_CLR(sk,&write_set);
                }
            }
        }
    }
    return 0;
}