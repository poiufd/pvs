#include <stdlib.h>
#include <stdio.h>
#include "server.h"


int main(int argc, char **argv) 
{ 
	if (argc != 2) 
	{
        printf("Usage: server <port>\nEx.:   server 8888\n");
        exit(0);
    }
	int port = atoi(argv[1]);
	run_server(port);
	return 0;
}