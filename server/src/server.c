#include "worker.h"
#include "socketutils.h"

int sig_num;
static void handler(int sig) { sig_num = sig;  }

void create_threads(int count, mqd_t** mq_fd, pthread_t** thread)
{
    int i;
    struct mq_attr attr;

    // pthread_attr_t tattr;
    // /* initialized with default attributes */
    // pthread_attr_init(&tattr);
    // pthread_attr_setdetachstate(&tattr,PTHREAD_CREATE_DETACHED);
    //queue attr
    attr.mq_maxmsg = SOMAXCONN;
    attr.mq_msgsize = sizeof(int);
    mode_t mode  = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
    *mq_fd = malloc(sizeof(mqd_t)*count);
    *thread = malloc(sizeof(pthread_t)*count);

    for (i = 0; i < count; i++)
    {
        char name[12];
        sprintf(name, "/thread%d", i); 
        if(((*mq_fd)[i] = mq_open(name, O_RDWR|O_CREAT|O_NONBLOCK, mode, &attr)) < 0)
        {
            perror("mq_open");
            exit(1);
        }
        if(pthread_create(&(*thread)[i], NULL, thread_handler, &(*mq_fd)[i]) < 0)
        {
            perror("thread");
            exit(1);
        }
    }   
}

void delete_threads(int count, mqd_t** mq_fd, pthread_t** thread)
{
    int i;
    for(i = 0; i < count; i++)
    {
        mq_close((*mq_fd)[i]);
        if (pthread_kill((*thread)[i],SIGUSR1) !=0)
            perror("thread kill");
        //mq_unlink("name");
    }
    //waiting threads termination
    for(i = 0; i < count; i++)
    {
        if (pthread_join((*thread)[i],NULL)!=0)
            perror("pthread join");
    }
    free(*mq_fd);
    free(*thread);
}

int run_server(int port) 
{   
    //struct sockaddr_in client_addr;
    struct sockaddr_in6 client_addr; 
    socklen_t addr_len;         
	int server_sk, client_sk;
	fd_set socket_set, ready_set, ready_write_set, write_set;
    mqd_t* mq_fd;  //queue array
    pthread_t* thread; 
    int cpu_count;   
    int worker_number = 0;
    struct sigaction sa;
    sigset_t emptyset, blockset;
    int on = 1;
    char send_buf[8] = "220 OK\n";

    //block sigint
    sigemptyset(&blockset);        
    sigaddset(&blockset, SIGINT);
    if (pthread_sigmask(SIG_BLOCK,&blockset,NULL) !=0)
        perror("sigmask");
    //establish signal handler
    sa.sa_handler = handler;        
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);
    sigaction(SIGINT, &sa, NULL);

    if ((cpu_count = get_nprocs()) < 0)
    {
        perror("cpu");
        exit(1);
    }
    cpu_count -= 1;

    create_server_socket(&server_sk, port);
    create_threads(cpu_count, &mq_fd, &thread);

    //clear set
   	FD_ZERO(&socket_set);
    FD_ZERO(&write_set);
   	//add fd to set
	FD_SET(server_sk, &socket_set);   
    addr_len = sizeof(client_addr);
    sigemptyset(&emptyset);
	while (1)
	{
		ready_set = socket_set;
        ready_write_set = write_set;
	 	
        if (pselect(FD_SETSIZE, &ready_set, &ready_write_set, NULL, NULL, &emptyset) < 0)
        {
            //signal caught
            if (errno == EINTR && sig_num == SIGINT)
            {
                printf("\ncaught SIGINT\n");
                delete_threads(cpu_count, &mq_fd, &thread);
                FD_CLR(server_sk,&socket_set);
                if (FD_ISSET(server_sk,&write_set))
                    FD_CLR(server_sk,&write_set);
                break;
            } 
            else
                perror("pselect");  
        } 
        else if (FD_ISSET(server_sk, &ready_set))
        {
            //incoming connection
            if ((client_sk = accept(server_sk, (struct sockaddr *) &client_addr, &addr_len)) < 0)
                perror("accept");
            if (ioctl(client_sk, FIOASYNC, (char *)&on) < 0)
                perror("client async");
             
            //strcpy(send_buf, "220 OK\n");
            if (send(client_sk, send_buf, 8, 0) < 0)
            {
                if (errno == EWOULDBLOCK)
                    FD_SET(client_sk,&write_set);
                else
                    perror("send");
            }

            printf("New connection: %d\n", client_sk);             
            char message[5];
            sprintf(message, "%d", client_sk); 
            printf("worker_number= %d\n", worker_number); 
            if(mq_send(mq_fd[worker_number], message, strlen(message)+1, 0) < 0)
                perror("mq_send"); 
            worker_number++;
            worker_number %= cpu_count;                  
        }
        else
        {
            for (int sk = 0; sk < FD_SETSIZE; sk++) 
            {
                if (FD_ISSET(sk, &ready_write_set)) 
                {
                    if (send(sk, send_buf, 8, 0))
                        FD_CLR(sk,&write_set);
                }
            }
        }
	}
    close(server_sk);
    return 0;
}