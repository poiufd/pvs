#include "smtp.h"
#include <stdio.h>
#include <string.h> 
#include <stdlib.h>
#include <pcre.h>
#include <sys/stat.h>
//#define PCRE_STATIC

void move_to_maildir(char* buf,char** username,int n)
{
	FILE* f;
	srand(time(NULL));
	for (int i = 0; i < n; i++)
	{
		time_t t = time(NULL);
    	struct tm *tm = localtime(&t);
    	char s[64];
    	strftime(s, sizeof(s), "%a%b%d%T", tm);
    	int r = rand();
        char prefix[100];
        struct stat st = {0};

        sprintf(prefix,"%s%s","/var/mail/",username[i]);
        printf("filedir = %s\n",prefix);
        
        if (stat(prefix, &st) == -1) 
        {	
        	printf("create dir\n");
        	mkdir(prefix, 0777);
        }

        strcat(prefix,"/new/");

        if (stat(prefix, &st) == -1) 
        {	
        	printf("create dir\n");
        	mkdir(prefix, 0777);
        }
        char prefix_2part[30];
        sprintf(prefix_2part,"%s%d%s",s,r,".txt");
        strcat(prefix,prefix_2part);

		f = fopen(prefix, "w");
		if (!f) 
		{ 
    		perror("file open failed");
		}
		fprintf(f,"%s", buf);
		fclose(f);
	}
}

void clear_socket_info(struct socket_info* sk_p)
{
	sk_p->data[0] = '\0';
	sk_p->data_length = 0;
	sk_p->n = 0;
}


void parce_command(char* buf, char* output, struct socket_info* sk_p)
{	
	char* state = sk_p->state;
	pcre *re_compiled_helo,*re_compiled_ehlo,*re_compiled_mail,
		*re_compiled_rcpt,*re_compiled_data,*re_compiled_data_body,
		*re_compiled_quit,*re_compiled_rset, *re_compiled_user;
  	int sub_str_vec[30];
  	const char *pcre_error_str;
  	int pcre_error_offset;
  	int pcre_exec_ret;
  	const char *username;
	char helo_reg[] = "^HELO\\s.+\r\n$";
	char ehlo_reg[] = "^EHLO\\s.+\r\n$";
	char mail_reg[] = "^MAIL\\sFROM:<.+>\r\n$";
	char rcpt_reg[] = "^RCPT\\sTO:<.+>\r\n$";
	char data_reg[] = "^DATA\r\n$";
	char data_body_reg[] = "^(.|\n)*\r\n\\.\r\n";
	char quit_reg[] = "^QUIT\r\n$";
	char rset_reg[] = "^RSET\r\n$";
	char user_reg[] = "\\<(.+)\\@";
	char succ[] = "250 OK\n";

	re_compiled_helo = pcre_compile((char *)helo_reg, 0, &pcre_error_str, &pcre_error_offset, NULL);
	re_compiled_ehlo = pcre_compile((char *)ehlo_reg, 0, &pcre_error_str, &pcre_error_offset, NULL);
	re_compiled_mail = pcre_compile((char *)mail_reg, 0, &pcre_error_str, &pcre_error_offset, NULL);
	re_compiled_rcpt = pcre_compile((char *)rcpt_reg, 0, &pcre_error_str, &pcre_error_offset, NULL);
	re_compiled_data = pcre_compile((char *)data_reg, 0, &pcre_error_str, &pcre_error_offset, NULL);
	re_compiled_data_body = pcre_compile((char *)data_body_reg, 0, &pcre_error_str, &pcre_error_offset, NULL);
	re_compiled_quit = pcre_compile((char *)quit_reg, 0, &pcre_error_str, &pcre_error_offset, NULL);
	re_compiled_rset = pcre_compile((char *)rset_reg, 0, &pcre_error_str, &pcre_error_offset, NULL);
	re_compiled_user = pcre_compile((char *)user_reg, 0, &pcre_error_str, &pcre_error_offset, NULL);

	if((re_compiled_helo == NULL) || (re_compiled_ehlo == NULL) ||
		(re_compiled_mail == NULL) || (re_compiled_rcpt == NULL) ||
		(re_compiled_data == NULL) || (re_compiled_quit == NULL) || 
		(re_compiled_rset == NULL) || (re_compiled_data_body == NULL) ||
		(re_compiled_user == NULL))
    	printf("could not compile %s\n", pcre_error_str);
  	
  	if(pcre_exec(re_compiled_helo,NULL,(char *)buf, strlen(buf),0,0,sub_str_vec,30)>0)
  	{
  		strcpy(state,"HELO");
  		strcpy(output,succ);
  	} 
  	else if (pcre_exec(re_compiled_ehlo,NULL,(char *)buf, strlen(buf),0,0,sub_str_vec,30)>0)
  	{
  		strcpy(state,"HELO");
  		strcpy(output,succ);
  	}
  	else if (pcre_exec(re_compiled_mail,NULL,(char *)buf, strlen(buf),0,0,sub_str_vec,30)>0 && 
  				(strcmp(state,"HELO")==0 || strcmp(state,"EHLO")==0) )
  	{
  	  	strcpy(state,"MAIL");
  		strcpy(output,succ);	
  	}
  	else if (pcre_exec(re_compiled_rcpt,NULL,(char *)buf, strlen(buf),0,0,sub_str_vec,30)>0 && 
  				(strcmp(state,"MAIL")==0 || strcmp(state,"RCPT")==0) )
  	{
  	  	strcpy(state,"RCPT");
  		strcpy(output,succ);
  		if ((pcre_exec_ret = pcre_exec(re_compiled_user,NULL,(char *)buf, strlen(buf),0,0,sub_str_vec,30))>0)
  		{
			pcre_get_substring(buf, sub_str_vec, pcre_exec_ret, 1, &(username));
        	printf("Match '%s'\n",username);
        	strcpy(sk_p->username[sk_p->n],username);
        	sk_p->n++;
        	pcre_free_substring(username);
  		}	
  	}
  	else if (pcre_exec(re_compiled_data,NULL,(char *)buf, strlen(buf),0,0,sub_str_vec,30)>0 && 
  				(strcmp(state,"RCPT")==0) )
  	{
  	  	strcpy(state,"DATA");
  		strcpy(output,"354 End data with <CR><LF>.<CR><LF>\n");	
  	}
  	else if (pcre_exec(re_compiled_data_body,NULL,(char *)buf, strlen(buf),0,0,sub_str_vec,30)>0 && 
  				(strcmp(state,"DATA")==0) )
  	{
  		strcpy(output,succ);
  		move_to_maildir(buf,sk_p->username,sk_p->n);
  		for(int i = 0; i < sk_p->n; i++)
  			printf("received username = %s\n",sk_p->username[i]);
  		clear_socket_info(sk_p);		
  	}  	
  	else if (pcre_exec(re_compiled_quit,NULL,(char *)buf, strlen(buf),0,0,sub_str_vec,30)>0 &&
  				(strcmp(state,"DATA")==0))
  	{
  		strcpy(state,"QUIT");
  		strcpy(output,"221 Bye\n");
  	}  
  	else
  	{
  		strcpy(output,"500 bad command\n");  		
  	}

  	// Free up the regular expression.
  	pcre_free(re_compiled_helo);
  	pcre_free(re_compiled_ehlo);
  	pcre_free(re_compiled_mail);
  	pcre_free(re_compiled_rcpt);
  	pcre_free(re_compiled_data);
  	pcre_free(re_compiled_data_body);
  	pcre_free(re_compiled_quit);
  	pcre_free(re_compiled_rset);
  	pcre_free(re_compiled_user);	

}